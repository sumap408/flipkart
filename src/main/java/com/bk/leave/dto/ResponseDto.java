package com.bk.leave.dto;

public class ResponseDto {
	private String status;
	private String statuscode;
	private String data;
	private String errorMessage;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public ResponseDto(String status, String statuscode, String data, String errorMessage) {
		super();
		this.status = status;
		this.statuscode = statuscode;
		this.data = data;
		this.errorMessage = errorMessage;
	}

}
