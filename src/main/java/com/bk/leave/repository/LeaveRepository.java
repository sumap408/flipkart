package com.bk.leave.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bk.leave.entity.LeaveDetails;

public interface LeaveRepository extends JpaRepository<LeaveDetails, Integer>{

}
